//Chip Rittler
//9.16.18
//Lab 02


public class Cyclometer {

  
  public static void main(String[] args) {
    
    //Set Variables
    double wheelDiameter=27.0,  // wheel diameter
  	PI=3.14159, // pi 
  	feetPerMile=5280,  // foot mile conversion
  	inchesPerFoot=12,   // inch foot conversion
  	secondsPerMinute=60;  // second minute conversion
	  double distanceTrip1, distanceTrip2,totalDistance;  //
    
    // input data: this data can be changed to varry the output
	  int secsTrip1=480;  // number of seconds 1
    int secsTrip2=3220;  // number of seconds 2
		int countsTrip1=1561;  // number of counts 1
		int countsTrip2=9037; // number of counts 2

    
    //Print output part 1
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
    // calculations
    distanceTrip1=countsTrip1*wheelDiameter*PI;
	  distanceTrip1/=inchesPerFoot*feetPerMile; 
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	  totalDistance=distanceTrip1+distanceTrip2;

    
    // print output part 2
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");

    

	}  
} 
