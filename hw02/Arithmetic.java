public class Arithmetic{
  
  public static void main(String args[]){

int numPants = 3; //Number of pairs of pants
double pantsPrice = 34.98; //Cost per pair of pants
int numShirts = 2; //Number of sweatshirts
double shirtPrice = 24.99; //Cost per shirt
int numBelts = 1; //Number of belts
double beltCost = 33.99; //cost per belt
double paSalesTax = 0.06; //the tax rate


    
double totalCostOfPants = (numPants * pantsPrice);   //total cost of pants
double totalCostOfShirts = (numShirts * shirtPrice);   //total cost of Shirts    
double totalCostOfBelts = (numBelts * beltCost);   //total cost of Belts
double totalTaxPants = (paSalesTax * totalCostOfPants);   //total Tax cost of Pants
double totalTaxShirts = (paSalesTax * totalCostOfShirts);   //total Tax cost of Shirts
double totalTaxBelts = (paSalesTax * totalCostOfBelts);   //total Tax cost of Belts
double totalCostNoTax = (totalCostOfPants + totalCostOfShirts + totalCostOfBelts);   //total cost of all items without Tax
double totalTax = (totalTaxPants + totalTaxShirts + totalTaxBelts);   //total cost of tax
double totalCostWithTax = (totalCostNoTax + totalTax);   //total cost of all items with Tax

totalCostOfPants = totalCostOfPants * 100;
totalCostOfShirts = totalCostOfShirts * 100;
totalCostOfBelts = totalCostOfBelts * 100;
totalTaxPants = totalTaxPants * 100;
totalTaxShirts = totalTaxShirts * 100;
totalTaxBelts = totalTaxBelts * 100;
totalCostNoTax = totalCostNoTax * 100;
totalTax = totalTax * 100;
totalCostWithTax = totalCostWithTax * 100;

   
totalCostOfPants = (int)(totalCostOfPants);
totalCostOfShirts = (int)(totalCostOfShirts);
totalCostOfBelts = (int)(totalCostOfBelts);
totalTaxPants = (int)(totalTaxPants);
totalTaxShirts = (int)(totalTaxShirts);
totalTaxBelts = (int)(totalTaxBelts);
totalCostNoTax = (int)(totalCostNoTax);
totalTax = (int)(totalTax);
totalCostWithTax = (int)(totalCostWithTax);

totalCostOfPants = totalCostOfPants /100;
totalCostOfShirts = totalCostOfShirts /100;
totalCostOfBelts = totalCostOfBelts /100;
totalTaxPants = totalTaxPants /100;
totalTaxShirts = totalTaxShirts /100;
totalTaxBelts = totalTaxBelts /100;
totalCostNoTax = totalCostNoTax /100;
totalTax = totalTax /100;
totalCostWithTax = totalCostWithTax /100;

    
    
System.out.println("The total cost of the pants is: "+totalCostOfPants);
System.out.println("The total cost of the shirts is: "+totalCostOfShirts);
System.out.println("The total cost of the belts is: "+totalCostOfBelts);
System.out.println("The total tax of the pants is: "+totalTaxPants);
System.out.println("The total tax of the shirts is: "+totalTaxShirts);
System.out.println("The total tax of the belts is: "+totalTaxBelts);
System.out.println("The total cost of all the items without tax is: "+totalCostNoTax);
System.out.println("The total tax of all the items is: "+totalTax);
System.out.println("The total cost of all the items with tax is: "+totalCostWithTax);

    
  }
  
}

