//Chip Rittler
//HW03

import java.util.Scanner;
public class Pyramid{
  
  public static void main(String args[]){

Scanner myScanner = new Scanner( System.in );
System.out.print("Enter the square side of the pyramid is (input length): ");
double squareSide = myScanner.nextDouble(); // enter the square side
    
System.out.print("Enter the height of the pyramid is (input height): ");
double height = myScanner.nextDouble(); // enter the height

double volume = ((Math.pow(2, squareSide)* height)/3); // calculating volume using (1/3)*B^2*H
System.out.println("The volume inside the pyramid is: "+volume); // print the volume
    
  } 
}

