//Chip Rittler
//HW03
// Conversion Source: http://www.kylesconverter.com/volume/acre--feet-to-cubic-miles#88212.1125


import java.util.Scanner;
public class convert{
  
  public static void main(String args[]){

Scanner myScanner = new Scanner( System.in );
System.out.print("Enter the number of acres of land affected by hurricane precipitation: ");
double acres = myScanner.nextDouble(); // enter total acres
    
System.out.print("Enter how many inches of rain were dropped on average: ");
double rain = myScanner.nextDouble(); // enter inches of rain

double totalRain = ((acres * rain) / 12); // calculating inches times acres and dividing by 12 to convert into feet times acres
double cubicMile = ((totalRain / 1000000)*0.2959); // converting the total feet of rain into cubic miles
System.out.println(cubicMile + "cubic miles"); // print the cubic miles
    
  } 
}

